package mu.zz.zagen.wordoftheday.model.Loaders;

import android.content.Context;
import android.os.Handler;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.sql.SQLException;

import mu.zz.zagen.wordoftheday.model.DictionariesDbManager;
import mu.zz.zagen.wordoftheday.model.WordsDbManager;

/**
 * Created by zagen on 1/24/2015.
 */
public class DictionaryLoader {

    private boolean isWork = true;
    private String mLoadedId = null;

    public String getLoadedId() {
        return mLoadedId;
    }


    public class Word{
        private String word;
        private String description;


        public String getWord() {
            return word;
        }

        public void setWord(String word) {
            this.word = word;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
    public interface OnLoadedListener{
        public void onLoaded(int words);
    }
    public Context getContext() {
        return mContext;
    }

    private Context mContext;
    private JSONReaderOpener mOpener;
    private OnLoadedListener mCallback = null;
    private Handler mMainHandler;
    private DictionariesDbManager mDictionariesManager;
    private WordsDbManager mWordsManager;

    public  DictionaryLoader(Context context, JSONReaderOpener opener){
        this.mContext = context;
        this.mOpener = opener;
        mMainHandler = new Handler(context.getMainLooper());

    }

    public void setOnLoadedListener(OnLoadedListener listener){
        this.mCallback = listener;
    }

    public void stopLoading(){
        this.isWork = false;
    }
    public void load(final String dicName, final String uri){
        new Thread(new Runnable() {
            private int mCount = 0;
            @Override
            public void run() {

                try {
                    mDictionariesManager = new DictionariesDbManager(mContext).open();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                try {
                    mWordsManager = new WordsDbManager(mContext).open();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                isWork = true;
                mCount = 0;
                Gson gson = new Gson();
                String dicId = "-1";

                if(mDictionariesManager != null && mWordsManager != null){
                    dicId = Long.toString(mDictionariesManager.addDictionary(dicName));
                    mLoadedId = dicId;
                }

                JsonReader jsonReader = mOpener.open(uri);
                try {
                    jsonReader.beginArray();
                    while (jsonReader.hasNext() && isWork) {

                        Word word = gson.fromJson(jsonReader,
                                Word.class);
                        if(word != null && !dicId.equals("-1")){
                            if(mWordsManager.addWord(word.getWord(), dicId, word.getDescription()) > 0){
                                mCount++;
                            }
                        }
                    }
                    jsonReader.endArray();
                    jsonReader.close();

                }catch (Exception e){}
                finally {
                    if(mWordsManager != null)
                        mWordsManager.close();
                    if(mDictionariesManager != null)
                        mDictionariesManager.close();
                }

                if(mCallback != null){
                    mMainHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            mCallback.onLoaded(mCount);
                        }
                    });

                }
            }
        }).start();
    }
}
