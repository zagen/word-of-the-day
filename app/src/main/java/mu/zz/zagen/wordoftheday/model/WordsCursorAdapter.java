package mu.zz.zagen.wordoftheday.model;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import mu.zz.zagen.wordoftheday.R;
import mu.zz.zagen.wordoftheday.WordOfTheDaySettings;

/**
 * Created by zagen on 1/23/2015.
 */
public class WordsCursorAdapter extends CursorAdapter{

    private final LayoutInflater mInflater;
    private final Context mContext;
    private int mSelectedPosition = -1;

    public WordsCursorAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = mInflater.inflate(R.layout.listfragment_word, parent, false);
        TextView wordTextView = (TextView)view.findViewById(R.id.id_listword_textview);
        wordTextView.setTypeface(WordOfTheDaySettings.getInstance(mContext).getNormalFont());
        if(cursor.getPosition() == mSelectedPosition)
            view.setBackgroundColor(Color.LTGRAY);
        else
            view.setBackgroundColor(Color.TRANSPARENT);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView wordTextView = (TextView)view.findViewById(R.id.id_listword_textview);
        String word = cursor.getString(cursor.getColumnIndex(WordsDatabaseHelper.KEY_WORDS_NAME));
        wordTextView.setText(word);
        if(cursor.getPosition() == mSelectedPosition)
            view.setBackgroundColor(Color.LTGRAY);
        else
            view.setBackgroundColor(Color.TRANSPARENT);

    }
    public void setSelected(int position){
        this.mSelectedPosition = position;
    }
}
