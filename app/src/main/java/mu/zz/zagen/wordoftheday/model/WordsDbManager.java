package mu.zz.zagen.wordoftheday.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;

import mu.zz.zagen.wordoftheday.WordOfTheDaySettings;

/**
 * Created by zagen on 1/23/2015.
 */
public class WordsDbManager {
    private WordsDatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;
    private Context mContext;

    public class Word {
        private String mName;
        private String mDescription;

        public String getDescription() {
            return mDescription;
        }

        public void setDescription(String description) {
            this.mDescription = description;
        }

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            this.mName = name;
        }
    }

    public WordsDbManager(Context context) {
        mContext = context;
    }

    public WordsDbManager open() throws SQLException {
        mDbHelper = new WordsDatabaseHelper(mContext);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public synchronized void close() {
        if (mDbHelper != null) {
            mDbHelper.close();
        }
    }

    public long addWord(String name, String dicId, String desc) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(WordsDatabaseHelper.KEY_WORDS_DIC, dicId);
        initialValues.put(WordsDatabaseHelper.KEY_WORDS_NAME, name);
        initialValues.put(WordsDatabaseHelper.KEY_WORDS_DESC, desc);
        return mDb.insert(WordsDatabaseHelper.TABLE_WORDS, null, initialValues);
    }

    public boolean deleteWord(String id) {
        WordOfTheDaySettings settings = WordOfTheDaySettings.getInstance(mContext);
        int doneDelete = 0;
        doneDelete = mDb.delete(WordsDatabaseHelper.TABLE_WORDS,
                WordsDatabaseHelper.KEY_WORDS_ID + "=?",
                new String[]{
                        id
                });
        if(doneDelete > 0 && settings.getTodayWord().equals(id))
            settings.resetTodayWord();
        return doneDelete > 0;
    }

    public Cursor fetchWordsFromDictionary(String dicId) throws SQLException {

        Cursor mCursor = mDb.query(WordsDatabaseHelper.TABLE_WORDS,
                null,
                WordsDatabaseHelper.KEY_WORDS_DIC + "=?",
                new String[]{
                        dicId
                }, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public boolean updateWord(String id, String dicId, String name, String desc) {
        ContentValues updatingValues = new ContentValues();
        updatingValues.put(WordsDatabaseHelper.KEY_WORDS_DIC, dicId);
        updatingValues.put(WordsDatabaseHelper.KEY_WORDS_NAME, name);
        updatingValues.put(WordsDatabaseHelper.KEY_WORDS_DESC, desc);
        return mDb.update(WordsDatabaseHelper.TABLE_WORDS,
                updatingValues,
                WordsDatabaseHelper.KEY_WORDS_ID + "=? ",
                new String[]{
                        id
                }
        ) == 1;
    }


    public Word fetchWordInfo(String id) {
        Word word = null;
        Cursor cursor = mDb.query(WordsDatabaseHelper.TABLE_WORDS,
                new String[]{
                        WordsDatabaseHelper.KEY_WORDS_NAME,
                        WordsDatabaseHelper.KEY_WORDS_DESC
                },
                WordsDatabaseHelper.KEY_WORDS_ID + "=?",
                new String[]{id}, null, null, null);
        if(cursor != null){
            cursor.moveToFirst();
            if(cursor.getCount() == 1)
            {
                word = new Word();
                word.setName(cursor.getString(cursor.getColumnIndex(WordsDatabaseHelper.KEY_WORDS_NAME)));
                word.setDescription(cursor.getString(cursor.getColumnIndex(WordsDatabaseHelper.KEY_WORDS_DESC)));
                return word;
            }
        }
        return word;
    }
}

