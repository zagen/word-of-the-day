package mu.zz.zagen.wordoftheday;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import mu.zz.zagen.wordoftheday.fragments.DictionaryOrderInfo.DictionaryOrderInfo;

/**
 * Created by zagen on 1/22/2015.
 */
public class NavigationDrawerAdapter extends ArrayAdapter<String> {
    private  String[] mItems;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null){
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view =  inflater.inflate(R.layout.listfragment_navigation_drawer_item, null);
        }
        String item = mItems[position];
        if (item != null) {
            Typeface typeface = WordOfTheDaySettings.getInstance(getContext()).getBoldFont();
            TextView drawerName = (TextView) view.findViewById(R.id.navigation_drawer_textview_item);
            try {
                drawerName.setText(item);
                drawerName.setTypeface(typeface);
            }catch (Exception e){}

        }
        return view;
    }

    public NavigationDrawerAdapter(Context context, int resource, String[] objects) {
        super(context, resource, objects);
        mItems = objects;
    }
}
