package mu.zz.zagen.wordoftheday.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import mu.zz.zagen.wordoftheday.FileListView;
import mu.zz.zagen.wordoftheday.R;
import mu.zz.zagen.wordoftheday.WordOfTheDaySettings;
import mu.zz.zagen.wordoftheday.model.Loaders.DictionaryLoader;
import mu.zz.zagen.wordoftheday.model.Loaders.JSONReaderOpenerFromFile;

/**
 * A simple {@link Fragment} subclass.

 */
public class LoadDictionaryFragment extends Fragment implements FileListView.OnDirectoryChangedListener,
                                                                FileListView.OnFileSelectedListener,
                                                                View.OnClickListener,
                                                                DictionaryLoader.OnLoadedListener
{

    public final static String CURRENT_PATH = "current_path";
    private FileListView mFileListView;
    private ImageButton mOkButton;

    private String mDicFullPath = null;
    private DictionaryLoader mLoader;
    private ProgressDialog mWaitingDialog;

    public LoadDictionaryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_load_dictionary, container, false);
        mFileListView = (FileListView)view.findViewById(R.id.file_list);
        mFileListView.setOnDirectoryChangedListener(this);
        mFileListView.setOnFileSelectedListener(this);
        mFileListView.setFilter("(.*)\\.json");

        mOkButton = (ImageButton)view.findViewById(R.id.ok_image_button);
        mOkButton.setEnabled(false);
        mOkButton.setOnClickListener(this);

        TextView titleTextView = (TextView)view.findViewById(R.id.title_text_view);
        titleTextView.setTypeface(WordOfTheDaySettings.getInstance(getActivity()).getBoldFont());
        mFileListView.setTitleTextView(titleTextView);
        mLoader = new DictionaryLoader(getActivity(), new JSONReaderOpenerFromFile());
        mLoader.setOnLoadedListener(this);

        mWaitingDialog = new ProgressDialog(getActivity());
        mWaitingDialog.setTitle(R.string.wait_please);
        mWaitingDialog.setMessage(getString(R.string.loading));
        mWaitingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mWaitingDialog.setCancelable(false);
        mWaitingDialog.setButton(Dialog.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (mLoader != null)
                    mLoader.stopLoading();
            }
        });
        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            String path = savedInstanceState.getString(CURRENT_PATH);
            mFileListView.setPath(path);
        }
    }

    @Override
    public void onDirectoryChanged(String path) {
        mOkButton.setEnabled(false);
        mDicFullPath = null;
    }

    @Override
    public void onFileSelected(String path) {
        mOkButton.setEnabled(true);
        mDicFullPath = path;
    }

    @Override
    public void onClick(View v) {
        if(mDicFullPath != null){
            String name = new File(mDicFullPath).getName();
            int pos = name.lastIndexOf(".");
            if (pos > 0) {
                name = name.substring(0, pos);
            }

            if(mWaitingDialog != null)
                mWaitingDialog.show();
            mLoader.load(name, mDicFullPath);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(CURRENT_PATH, mFileListView.getPath());
    }

    @Override
    public void onLoaded(int words) {
        if(mWaitingDialog != null && mWaitingDialog.isShowing()){
            mWaitingDialog.hide();
        }
        Toast.makeText(getActivity(),getString(R.string.count_of_loaded_word) + Integer.toString(words), Toast.LENGTH_SHORT).show();
    }
}
