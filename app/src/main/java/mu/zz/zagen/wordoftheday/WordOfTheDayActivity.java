package mu.zz.zagen.wordoftheday;

import android.app.ProgressDialog;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.widget.DrawerLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import mu.zz.zagen.wordoftheday.model.CurrentDictionaryDbManager;
import mu.zz.zagen.wordoftheday.model.DictionariesDbManager;
import mu.zz.zagen.wordoftheday.model.Loaders.DictionaryLoader;
import mu.zz.zagen.wordoftheday.model.Loaders.JSONReaderOpenerFromAssets;


public class WordOfTheDayActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private DrawerLayout mDrawerLayout;

    private InterstitialAd mInterstitial;


    private final static String VISIBLE_FRAGMENT_TAG = "VISIBLE_FRAGMENT";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word_of_the_day);


        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        DictionariesDbManager dictionariesDbManager = null;
        try{
            dictionariesDbManager = new DictionariesDbManager(getApplicationContext()).open();
            int count = dictionariesDbManager.fetchAllDictionaries().size();
            if(count <= 0){
                ProgressDialog progress = new ProgressDialog(this);
                progress.setMessage(getText(R.string.loading));
                progress.setCancelable(false);
                new FirstLoad(progress, this).execute();
            }

        }catch (Exception e){}
        finally {
            if(dictionariesDbManager != null)
                dictionariesDbManager.close();
        }
        // Create the interstitial.
        mInterstitial = new InterstitialAd(this);
        mInterstitial.setAdUnitId(getString(R.string.interstatial_ad_unit_id));

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        // Create ad request.
        AdRequest adRequest = new AdRequest.Builder().build();

        // Begin loading your interstitial.
        mInterstitial.loadAd(adRequest);
        mInterstitial.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                displayInterstitial();
            }
        });

    }
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            if(!mNavigationDrawerFragment.isDrawerOpen()){

                mDrawerLayout.openDrawer(Gravity.LEFT);
            }else{
                mDrawerLayout.closeDrawer(Gravity.LEFT);
            }

            return true;
        }
        return super.onKeyUp(keyCode, event);
    }
    public class FirstLoad extends AsyncTask<Void, Void, Void> {
        ProgressDialog progress;
        Context context;
        public FirstLoad(ProgressDialog progress, Context context) {
            this.progress = progress;
            this.context = context;
        }

        public void onPreExecute() {
            progress.show();
        }

        public Void doInBackground(Void... unused) {
            final DictionaryLoader loader = new DictionaryLoader(context, new JSONReaderOpenerFromAssets(context));
            loader.setOnLoadedListener(new DictionaryLoader.OnLoadedListener() {
                @Override
                public void onLoaded(int words) {
                    CurrentDictionaryDbManager currentDictionaryDbManager = null;
                    try{
                        currentDictionaryDbManager = new CurrentDictionaryDbManager(context).open();
                        currentDictionaryDbManager.moveAllDicWordsToCurrent(loader.getLoadedId(), new CurrentDictionaryDbManager.OnLoadCompleteListener() {
                            @Override
                            public void onLoadComplete() {
                                Fragment frg = null;
                                frg = getSupportFragmentManager().findFragmentByTag(VISIBLE_FRAGMENT_TAG);
                                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                                ft.detach(frg);
                                ft.attach(frg);
                                ft.commit();
                                progress.dismiss();
                            }
                        });
                    }catch (Exception e){}
                    finally {
                        if(currentDictionaryDbManager != null)
                            currentDictionaryDbManager.close();
                    }
                }
            });
            loader.load(context.getString(R.string.dic_clever_words), "clever.json");
            return null;
        }

        public void onPostExecute(Void unused) {
            //progress.dismiss();
        }
    }


    @Override
    public void onNavigationDrawerItemSelected(int position) {
        String[] items = getResources().getStringArray(R.array.navigation_drawer_string_array);
        String name = items[position];
        mTitle = name;
        Fragment fragment = FragmentChooser.chooseFragment(this, name);
        if(fragment != null){
            // update the main content by replacing fragments
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, fragment, VISIBLE_FRAGMENT_TAG)
                    .commit();
        }

    }


    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
        
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            //getMenuInflater().inflate(R.menu.current_word, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //send word to widget
        Intent intent = new Intent(this, WordOfTheDayWidget.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        int ids[] = AppWidgetManager.getInstance(this.getApplication()).getAppWidgetIds(
                new ComponentName(this.getApplication(),
                        WordOfTheDayWidget.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,ids);
        sendBroadcast(intent);
    }

    // Invoke displayInterstitial() when you are ready to display an interstitial.
    public void displayInterstitial() {
        if (mInterstitial.isLoaded()) {
            mInterstitial.show();
        }
    }
}
