package mu.zz.zagen.wordoftheday.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by zagen on 1/23/2015.
 */
public class WordsDatabaseHelper extends SQLiteOpenHelper {
    public final static String DATABASE_NAME = "wordsdb";
    public final static int DATABASE_VERSION = 1;

    public final static String TABLE_WORDS = "words";
    public final static String TABLE_DICTIONARIES = "dictionaries";
    public final static String TABLE_CURRENT_DICTIONARY = "current_dictionary";

    public final static String KEY_CURRENT_DICTIONARY_ID = "word";

    public final static String KEY_DICTIONARIES_ID = "_id";
    public final static String KEY_DICTIONARIES_NAME = "name";

    public final static String KEY_WORDS_ID = "_id";
    public final static String KEY_WORDS_DIC = "dictionary";
    public final static String KEY_WORDS_NAME = "name";
    public final static String KEY_WORDS_DESC = "description";

    private final static String SQL_CREATE_TABLE_DICTIONARIES = String.format(
            "CREATE TABLE IF NOT EXISTS %s ( " +
            "    %s  INTEGER PRIMARY KEY AUTOINCREMENT," +
            "    %s  TEXT    NOT NULL " +
            ");", TABLE_DICTIONARIES, KEY_DICTIONARIES_ID, KEY_DICTIONARIES_NAME);

    private final static String SQL_CREATE_TABLE_WORDS = String.format(
            "CREATE TABLE IF NOT EXISTS %s ( " +
                    "    %s         INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "    %s         INTEGER NOT NULL" +
                    "                        REFERENCES dictionaries ( _id ) ON DELETE CASCADE" +
                    "                                                        ON UPDATE CASCADE," +
                    "    %s         TEXT    NOT NULL," +
                    "    %s         TEXT    NOT NULL " +
                    ");",
            TABLE_WORDS,
            KEY_WORDS_ID,
            KEY_WORDS_DIC,
            KEY_WORDS_NAME,
            KEY_WORDS_DESC);

    private final static String SQL_CREATE_TABLE_CURRENT_DIC = String.format(
            "CREATE TABLE IF NOT EXISTS %s ( " +
                    "     %s INTEGER PRIMARY KEY" +
                    "                 NOT NULL" +
                    "                 REFERENCES words ( _id ) ON DELETE CASCADE" +
                    "                                          ON UPDATE CASCADE " +
                    ");",
            TABLE_CURRENT_DICTIONARY,
            KEY_CURRENT_DICTIONARY_ID
    );
    private final static String SQL_DROP_TABLE_WORDS = String.format(
            "DROP TABLE IF EXISTS %s",
            TABLE_WORDS
            );
    private final static String SQL_DROP_TABLE_DICTIONARIES = String.format(
            "DROP TABLE IF EXISTS %s",
            TABLE_DICTIONARIES
            );
    private final static String SQL_DROP_TABLE_CURRENT_DIC = String.format(
            "DROP TABLE IF EXISTS %s",
            TABLE_CURRENT_DICTIONARY
            );



    public WordsDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_DICTIONARIES);
        db.execSQL(SQL_CREATE_TABLE_WORDS);
        db.execSQL(SQL_CREATE_TABLE_CURRENT_DIC);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DROP_TABLE_CURRENT_DIC);
        db.execSQL(SQL_DROP_TABLE_DICTIONARIES);
        db.execSQL(SQL_DROP_TABLE_WORDS);
        onCreate(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        db.execSQL("PRAGMA foreign_keys=ON");
    }
}
