package mu.zz.zagen.wordoftheday.model.Loaders;

import com.google.gson.stream.JsonReader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by zagen on 1/24/2015.
 */
public class JSONReaderOpenerFromFile implements JSONReaderOpener{

    @Override
    public JsonReader open(String uri) {
        try
        {
            //BufferedReader br = new BufferedReader(new FileReader(uri));
            return new JsonReader(new FileReader(uri));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
