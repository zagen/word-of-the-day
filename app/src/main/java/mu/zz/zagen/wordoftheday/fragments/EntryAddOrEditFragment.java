package mu.zz.zagen.wordoftheday.fragments;


import android.app.AlertDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import mu.zz.zagen.wordoftheday.AutoResizeTextView;
import mu.zz.zagen.wordoftheday.R;
import mu.zz.zagen.wordoftheday.WordOfTheDayActivity;
import mu.zz.zagen.wordoftheday.WordOfTheDaySettings;
import mu.zz.zagen.wordoftheday.model.DictionariesDbManager;
import mu.zz.zagen.wordoftheday.model.WordsDbManager;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EntryAddOrEditFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EntryAddOrEditFragment extends Fragment implements View.OnClickListener {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_DIC_ID = "dictionary";
    private static final String ARG_ENTRY_ID = "entry_id";
    private static final String ARG_RETURN_TO = "return_to";
    private static final String INPUT_WORD = "input_word";
    private static final String INPUT_DESC = "input_desc";


    private String mDictionaryId;
    private String mEntryId;

    private EditText mWordEditText;

    private EditText mDescEditText;
    private WordsDbManager mWordsManager;
    private ImageButton mOkButton;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param dicId Name of dictionary from where we are taking entry.
     * @param entryId Id for editing entry
     * @return A new instance of fragment EntryAddOrEditFragment.
     */
    public static EntryAddOrEditFragment newInstance(String dicId, String entryId) {
        EntryAddOrEditFragment fragment = new EntryAddOrEditFragment();
        Bundle args = new Bundle();
        args.putString(ARG_DIC_ID, dicId);
        args.putString(ARG_ENTRY_ID, entryId);
        fragment.setArguments(args);
        return fragment;
    }
    public static EntryAddOrEditFragment newInstance(String dicId, String entryId, int returnTo) {
        EntryAddOrEditFragment fragment = newInstance(dicId, entryId);
        fragment.getArguments().putInt(ARG_RETURN_TO, returnTo);
        return fragment;
    }
    public EntryAddOrEditFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Bundle args = getArguments();
            if(args.containsKey(ARG_DIC_ID))
                mDictionaryId = getArguments().getString(ARG_DIC_ID);
            if(args.containsKey(ARG_ENTRY_ID))
                mEntryId = getArguments().getString(ARG_ENTRY_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_entry_add_or_edit, container, false);

        ImageButton cancelButton = (ImageButton)view.findViewById(R.id.cancel_image_button);
        cancelButton.setOnClickListener(this);
        Typeface boldTF = WordOfTheDaySettings.getInstance(getActivity()).getBoldFont();
        Typeface regularTF = WordOfTheDaySettings.getInstance(getActivity()).getNormalFont();

        AutoResizeTextView dictionaryTextView = (AutoResizeTextView)view.findViewById(R.id.dictionary_name_text_view);
        dictionaryTextView.setTypeface(boldTF);

        TextView wordTitleTextView = (TextView)view.findViewById(R.id.word_title_text_view);
        wordTitleTextView.setTypeface(boldTF);
        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if(mOkButton != null){
                    if(isInputCorrect()){
                        mOkButton.setEnabled(true);
                    }else
                        mOkButton.setEnabled(false);
                }
            }
        };

        mWordEditText = (EditText)view.findViewById(R.id.word_edit_text);
        mWordEditText.setTypeface(regularTF);
        mWordEditText.addTextChangedListener(watcher);

        TextView descTitleTextView = (TextView)view.findViewById(R.id.desc_title_text_view);
        descTitleTextView.setTypeface(boldTF);

        mDescEditText = (EditText)view.findViewById(R.id.description_edit_text);
        mDescEditText.setTypeface(regularTF);
        mDescEditText.addTextChangedListener(watcher);

        String dicName = "";
        try{
            DictionariesDbManager dicManager = new DictionariesDbManager(getActivity()).open();
            dicName = dicManager.fetchDictionaryName(mDictionaryId);
            dictionaryTextView.setText(dicName);
            mWordsManager =  new WordsDbManager(getActivity()).open();
            if(!mEntryId.equals("-1")){
                WordsDbManager.Word word = mWordsManager.fetchWordInfo(mEntryId);
                if(word != null){
                    mWordEditText.setText(word.getName());
                    mDescEditText.setText(word.getDescription());
                }
            }


        }catch (Exception e){};

        mOkButton = (ImageButton)view.findViewById(R.id.ok_image_button);
        mOkButton.setEnabled(isInputCorrect());
        mOkButton.setOnClickListener(this);
        return view;
    }

    private void back(){
        Bundle bundle = new Bundle();
        bundle.putString(DictionariesFragment.CURRENT_DICTIONARY, mDictionaryId);
        if(getArguments().containsKey(ARG_RETURN_TO))
            bundle.putInt(DictionariesFragment.CURRENT_WORD_INDEX, getArguments().getInt(ARG_RETURN_TO));

        WordOfTheDayActivity activity = (WordOfTheDayActivity)getActivity();
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        Fragment fragment = new DictionariesFragment();
        fragment.setArguments(bundle);
        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    private boolean isInputCorrect(){
        return mWordEditText.getText().toString().trim().length() > 0
                && mDescEditText.getText().toString().trim().length() >0;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.cancel_image_button){
            back();
        }else if(v.getId() == R.id.ok_image_button){
            if(mEntryId.equals("-1")){
                long id = -1;
                try{
                    id = mWordsManager.addWord(
                            mWordEditText.getText().toString(),
                            mDictionaryId,
                            mDescEditText.getText().toString());
                }catch (Exception e){}
                if(id == -1){
                    AlertDialog.Builder errorDialogBuilder =
                            new AlertDialog.Builder(getActivity());
                    errorDialogBuilder.setTitle(getString(R.string.error))
                            .setMessage(getString(R.string.error_put_word_in_dic))
                            .setPositiveButton("OK", null).show();

                }else{
                    back();
                }
            }else {
                boolean isUpdated = false;
                try{
                    isUpdated = mWordsManager.updateWord(
                            mEntryId,
                            mDictionaryId,
                            mWordEditText.getText().toString(),
                            mDescEditText.getText().toString());
                }catch (Exception e){}
                if(!isUpdated){
                    AlertDialog.Builder errorDialogBuilder =
                            new AlertDialog.Builder(getActivity());
                    errorDialogBuilder.setTitle(getString(R.string.error))
                            .setMessage(getString(R.string.error_put_word_in_dic))
                            .setPositiveButton("OK", null).show();

                }else{
                    back();
                }
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(INPUT_WORD, mWordEditText.getText().toString());
        outState.putString(INPUT_DESC, mDescEditText.getText().toString());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null)
        {
            mWordEditText.setText(savedInstanceState.getString(INPUT_WORD));
            mDescEditText.setText(savedInstanceState.getString(INPUT_DESC));
        }
    }
}
