package mu.zz.zagen.wordoftheday;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.widget.RemoteViews;

import mu.zz.zagen.wordoftheday.model.CurrentDictionaryDbManager;
import mu.zz.zagen.wordoftheday.model.WordsDbManager;


/**
 * Implementation of App Widget functionality.
 */
public class WordOfTheDayWidget extends AppWidgetProvider {
    private static final String UPDATE_CLICKED = "wordOfTheDayWidgetUpdateButtonClick";
    //public static final String MORE_INFO_CLICKED    = "wordOfTheDayWidgetMoreInfoButtonClick";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them

        CurrentDictionaryDbManager currentDicWordsManager = null;
        try {

            WordOfTheDaySettings settings = WordOfTheDaySettings.getInstance(context);
            currentDicWordsManager = new CurrentDictionaryDbManager(context).open();

            if(!settings.isTodayWordAlive()){
                String wordId = currentDicWordsManager.fetchRandomWordId();
                if (wordId != null) {
                    settings.setTodayWord(wordId);
                }
            }

        } catch (Exception e) {
        }finally {
            if(currentDicWordsManager != null)
                currentDicWordsManager.close();
        }
        final int N = appWidgetIds.length;
        for (int i = 0; i < N; i++) {
            updateAppWidget(context, appWidgetManager, appWidgetIds[i]);
        }
    }


    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {


        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.word_of_the_day_widget);

        ///make bitmap for 'Подробнее..'
        String text = context.getString(R.string.more_information);

        Resources res = context.getResources();
        Bitmap moreInfoBitmap = Bitmap.createBitmap(
                res.getDimensionPixelSize(R.dimen.widget_width),
                res.getDimensionPixelSize(R.dimen.widget_bar_height_medium),
                Bitmap.Config.ARGB_4444);
        Canvas moreInfoCanvas = new Canvas(moreInfoBitmap);

        Typeface tf = WordOfTheDaySettings.getInstance(context).getNormalFont();

        TextPaint mTextPaint=new TextPaint();
        mTextPaint.setAntiAlias(true);
        mTextPaint.setSubpixelText(true);
        mTextPaint.setTypeface(tf);
        mTextPaint.setColor(Color.BLACK);
        mTextPaint.setFakeBoldText(true);
        mTextPaint.setTextSize(context.getResources().getDimensionPixelSize(R.dimen.widget_text_size_about));
        StaticLayout mTextLayout = new StaticLayout(
                text,
                mTextPaint, moreInfoCanvas.getWidth(),
                Layout.Alignment.ALIGN_NORMAL,
                1.0f,
                0.0f,
                false);

        moreInfoCanvas.save();
        moreInfoCanvas.translate(0, context.getResources().getDimensionPixelSize(R.dimen.widget_footer_text_top_padding));
        mTextLayout.draw(moreInfoCanvas);
        moreInfoCanvas.restore();


        //take word and draw it
        try{
            updateWord(views, context);
            views.setImageViewBitmap(R.id.widget_about, moreInfoBitmap);
        }catch (Exception e){}

        Intent intent = new Intent(context, WordOfTheDayActivity.class);
        PendingIntent openActivityPendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        views.setOnClickPendingIntent(R.id.widget_reload_button, getPendingSelfIntent(context, UPDATE_CLICKED));
        views.setOnClickPendingIntent(R.id.widget_about, openActivityPendingIntent);
        views.setOnClickPendingIntent(R.id.widget_word_image, openActivityPendingIntent);
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        if (UPDATE_CLICKED.equals(intent.getAction())) {

            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

            RemoteViews remoteViews;

            remoteViews = new RemoteViews(context.getPackageName(), R.layout.word_of_the_day_widget);
            ComponentName widget = new ComponentName(context, WordOfTheDayWidget.class);

            loadNewWord(context);
            updateWord(remoteViews, context);

            appWidgetManager.updateAppWidget(widget, remoteViews);

        }
    }

    protected PendingIntent getPendingSelfIntent(Context context, String action) {
        Intent intent = new Intent(context, getClass());
        intent.setAction(action);
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }
    private void updateWord(RemoteViews views,Context context)
    {
        WordsDbManager.Word word = null;
        WordsDbManager wordsManager = null;
        WordOfTheDaySettings settings = WordOfTheDaySettings.getInstance(context);

        try{
            wordsManager = new WordsDbManager(context).open();
            String wordId = settings.getTodayWord();
            word = wordsManager.fetchWordInfo(wordId);

        }catch (Exception e){}
        finally {
            if(wordsManager != null)
                wordsManager.close();
        }

        if(word == null){
            try {
                loadNewWord(context);
                String wordId = settings.getTodayWord();
                wordsManager = new WordsDbManager(context).open();
                word = wordsManager.fetchWordInfo(wordId);
            }catch (Exception e){}
            finally {
                if(wordsManager != null)
                    wordsManager.close();
            }
        }

        String text = context.getString(R.string.add_words);
        if(word != null){
            text = word.getName();
        }

        final int maxLength = 18;
        if(text.length() > maxLength)
            text = text.substring(0, maxLength) + "...";
        Resources res = context.getResources();
        Bitmap btmText = Bitmap.createBitmap(
                res.getDimensionPixelSize(R.dimen.widget_width),
                res.getDimensionPixelSize(R.dimen.widget_word_height_medium),
                Bitmap.Config.ARGB_4444);
        Canvas cnvText = new Canvas(btmText);

        Typeface tf = WordOfTheDaySettings.getInstance(context).getBoldFont();

        TextPaint mTextPaint=new TextPaint();

        mTextPaint.setAntiAlias(true);
        mTextPaint.setSubpixelText(true);
        mTextPaint.setTypeface(tf);
        mTextPaint.setColor(Color.BLACK);
        mTextPaint.setTextSize(context.getResources().getDimensionPixelSize(R.dimen.widget_text_size));
        StaticLayout mTextLayout = new StaticLayout(
                text,
                mTextPaint, cnvText.getWidth(),
                Layout.Alignment.ALIGN_CENTER,
                1.0f,
                0.0f,
                false);

        cnvText.save();

        mTextLayout.draw(cnvText);
        cnvText.restore();

        views.setImageViewBitmap(R.id.widget_word_image, btmText);

    }
    protected boolean loadNewWord(Context context){
        boolean loaded = false;
        WordOfTheDaySettings settings = WordOfTheDaySettings.getInstance(context);
        CurrentDictionaryDbManager currentDicWordsManager = null;
        try {
            currentDicWordsManager = new CurrentDictionaryDbManager(context).open();
            String wordId = currentDicWordsManager.fetchRandomWordId();
            if (wordId != null) {
                settings.setTodayWord(wordId);
                loaded = true;
            }
        } catch (Exception e) {
        }finally {
            if(currentDicWordsManager != null)
                currentDicWordsManager.close();
        }
        if(!loaded)
            settings.resetTodayWord();
        return loaded;
    }

}


