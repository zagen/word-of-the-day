package mu.zz.zagen.wordoftheday.fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import mu.zz.zagen.wordoftheday.R;
import mu.zz.zagen.wordoftheday.model.CurrentDictionaryDbManager;
import mu.zz.zagen.wordoftheday.model.DictionariesDbManager;
import mu.zz.zagen.wordoftheday.model.SpinnerStringAdapter;
import mu.zz.zagen.wordoftheday.WordOfTheDaySettings;
import mu.zz.zagen.wordoftheday.model.WordsCursorAdapter;
import mu.zz.zagen.wordoftheday.model.WordsDatabaseHelper;
import mu.zz.zagen.wordoftheday.model.WordsDbManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class CurrentDictionaryFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener {


    /**
     * Spinner for all dictionary list
     */
    private Spinner mDicSpinner;

    /**
     * listview, contain list of words for selected
     */
    private ListView mWordsList;
    private WordsDbManager mWordsDbManager;

    private ListView mCurrentWordsList;

    private String[] mDictionariesName = new String[]{};
    private String[] mDictionariesIds = new String[]{};

    private ImageButton mAddImageButton;
    private ImageButton mDeleteImageButton;

    private long mSelectedCurrentWordId = -1;
    private long mSelectedWordId = -1;

    private CurrentDictionaryDbManager mCurrentWordsManager = null;

    private boolean isHeavyWorkDoing = false;
    private ProgressDialog mWaitingDialog;


    /**
     * transform list of dictionary objects to 2 arrays of names and ids
     *
     * @param dics arrays of dictionaries
     */
    private void loadDictionaries(List<DictionariesDbManager.Dic> dics) {
        int count = dics.size();
        if (dics != null) {
            mDictionariesName = new String[count];
            mDictionariesIds = new String[count];
            int index = 0;
            for (DictionariesDbManager.Dic dic : dics) {
                mDictionariesName[index] = dic.getName();
                mDictionariesIds[index] = dic.getId();
                index++;
            }
        }
    }

    public CurrentDictionaryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_current_dictionary, container, false);

        //take listView
        mWordsList = (ListView) view.findViewById(R.id.dictionary_content_list);
        WordsCursorAdapter listWordsAdapter = new WordsCursorAdapter(getActivity(), null, true);
        mWordsList.setAdapter(listWordsAdapter);

        mWordsList.setOnItemSelectedListener(this);
        mWordsList.setOnItemClickListener(this);

        WordsCursorAdapter listCurrentWordsAdapter = new WordsCursorAdapter(getActivity(), null, true);
        mCurrentWordsList = (ListView) view.findViewById(R.id.current_dictionary_content_list);
        mCurrentWordsList.setAdapter(listCurrentWordsAdapter);
        mCurrentWordsList.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        mCurrentWordsList.setOnItemClickListener(this);

        try {
            DictionariesDbManager adapter = new DictionariesDbManager(getActivity());
            adapter.open();
            loadDictionaries(adapter.fetchAllDictionaries());
            adapter.close();
        } catch (Exception e) {
        }

        try {
            //manager for current words
            mCurrentWordsManager = new CurrentDictionaryDbManager(getActivity()).open();
        } catch (Exception e) {
        }

        try {
            //create manager for words
            mWordsDbManager = new WordsDbManager(getActivity()).open();
        } catch (Exception e) {
        }

        mDicSpinner = (Spinner) view.findViewById(R.id.dictionaries_spinner);
        ArrayAdapter<String> spinnerAdapter = new SpinnerStringAdapter(getActivity(), R.layout.listfragment_spinner, mDictionariesName);

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mDicSpinner.setAdapter(spinnerAdapter);
        mDicSpinner.setOnItemSelectedListener(this);

        mAddImageButton = (ImageButton) view.findViewById(R.id.add_entry_image_button);
        mAddImageButton.setOnClickListener(this);
        mAddImageButton.setEnabled(false);

        mDeleteImageButton = (ImageButton) view.findViewById(R.id.remove_entry_image_button);
        mDeleteImageButton.setOnClickListener(this);
        mDeleteImageButton.setEnabled(false);

        TextView currentDicTextView = (TextView) view.findViewById(R.id.current_dictionary_textview);
        currentDicTextView.setTypeface(WordOfTheDaySettings.getInstance(getActivity()).getBoldFont());

        ImageButton addAllButton = (ImageButton) view.findViewById(R.id.add_all_entry_image_button);
        addAllButton.setOnClickListener(this);

        ImageButton removeAllButton = (ImageButton) view.findViewById(R.id.remove_all_entry_image_button);
        removeAllButton.setOnClickListener(this);

        mWaitingDialog = new ProgressDialog(getActivity());
        mWaitingDialog.setTitle(R.string.wait_please);
        mWaitingDialog.setMessage(getString(R.string.loading));
        mWaitingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mWaitingDialog.setCancelable(false);
        mWaitingDialog.setButton(Dialog.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (mCurrentWordsManager != null)
                    mCurrentWordsManager.stop();
            }
        });

        loadSelectedDictionary();
        loadCurrentDictionary();
        return view;
    }

    private void loadSelectedDictionary() {
        Cursor dictionariesCursor = null;
        try {
            setSelectedDicWord(-1);
            int spinnerIndex = mDicSpinner.getSelectedItemPosition();
            dictionariesCursor = mWordsDbManager.fetchWordsFromDictionary(mDictionariesIds[spinnerIndex]);
            CursorAdapter adapter = (CursorAdapter) mWordsList.getAdapter();
            if (dictionariesCursor != null)
                adapter.changeCursor(dictionariesCursor);
        } catch (Exception ex) {
        }

    }

    private void loadCurrentDictionary() {
        Cursor wordsCursor = null;
        setSelectedCurrentDicWord(-1);
        try {
            wordsCursor = mCurrentWordsManager.fetchAllCurrentDictionaryWords();
            CursorAdapter adapter = (CursorAdapter) mCurrentWordsList.getAdapter();
            if (wordsCursor != null)
                adapter.changeCursor(wordsCursor);
        } catch (Exception ex) {
        }
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.add_entry_image_button) {
            try {
                if (mCurrentWordsManager.isWordInCurrentAlready(Long.toString(mSelectedWordId))) {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(R.string.error)
                            .setMessage(R.string.error_word_already_current)
                            .setPositiveButton(R.string.ok, null)
                            .show();
                } else {
                    long id = -1;
                    String selectedID = Long.toString(mSelectedWordId);
                    id = mCurrentWordsManager.addCurrentDictionaryWord(selectedID);
                    if (id == -1) {
                        new AlertDialog.Builder(getActivity())
                                .setTitle(R.string.error)
                                .setMessage(R.string.error_current_word_add)
                                .setPositiveButton(R.string.ok, null)
                                .show();
                    } else {
                        if (mAddImageButton != null)
                            mAddImageButton.setEnabled(false);
                        setSelectedDicWord(-1);
                        loadCurrentDictionary();
                    }
                }
            } catch (Exception e) {
            }
        } else if (v.getId() == R.id.remove_entry_image_button) {
            try {
                if (mCurrentWordsManager.deleteWord(Long.toString(mSelectedCurrentWordId))) {
                    loadCurrentDictionary();
                    setSelectedCurrentDicWord(-1);
                    if (mDeleteImageButton != null)
                        mDeleteImageButton.setEnabled(false);
                } else {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(R.string.error)
                            .setMessage(R.string.error_current_word_delete)
                            .setPositiveButton(R.string.ok, null)
                            .show();
                }
            } catch (Exception e) {
            }
        } else if (v.getId() == R.id.add_all_entry_image_button) {
            if (mDicSpinner.getAdapter().getCount() > 0)
                try {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(R.string.add_conformation)
                            .setMessage(R.string.add_question)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //got here if user accept add selected dictionary's words to current

                                    //check if list of dics isnt empty
                                    final int position = mDicSpinner.getSelectedItemPosition();
                                    if (position != -1) {
                                        final String idDic = mDictionariesIds[position];
                                        new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                mCurrentWordsManager.moveAllDicWordsToCurrent(idDic, new CurrentDictionaryDbManager.OnLoadCompleteListener() {
                                                    @Override
                                                    public void onLoadComplete() {
                                                        if (mWaitingDialog != null) {
                                                            mWaitingDialog.dismiss();
                                                        }
                                                        loadCurrentDictionary();
                                                    }
                                                });
                                            }
                                        }).start();

                                        if (mWaitingDialog != null) {
                                            mWaitingDialog.show();
                                        }

                                    }
                                }
                            })
                            .setNegativeButton(R.string.cancel, null)
                            .show();

                } catch (Exception e) {
                }
        } else if (v.getId() == R.id.remove_all_entry_image_button) {
            if (mCurrentWordsList.getCount() > 0)
                try {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(R.string.delete_conformation)
                            .setMessage(R.string.delete_current_dic_question)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (mCurrentWordsManager != null &&
                                            mCurrentWordsManager.deleteAllCurrentDictionaryWords()) {
                                        loadCurrentDictionary();
                                    }
                                }
                            })
                            .setNegativeButton(R.string.cancel, null)
                            .show();

                } catch (Exception e) {
                }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        loadSelectedDictionary();
        if (mAddImageButton != null)
            mAddImageButton.setEnabled(false);
        if (mDeleteImageButton != null)
            mDeleteImageButton.setEnabled(false);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long arg3) {

        for (int a = 0; a < parent.getChildCount(); a++) {
            parent.getChildAt(a).setBackgroundColor(Color.TRANSPARENT);
        }
        if (parent.getId() == R.id.dictionary_content_list) {
            setSelectedDicWord(position);
            if (mAddImageButton != null)
                mAddImageButton.setEnabled(true);
        } else if (parent.getId() == R.id.current_dictionary_content_list) {
            setSelectedCurrentDicWord(position);
            if (mDeleteImageButton != null)
                mDeleteImageButton.setEnabled(true);
        }
        view.setBackgroundColor(Color.LTGRAY);
    }

    private void setSelectedDicWord(int pos) {
        mSelectedWordId = (int) mWordsList.getAdapter().getItemId(pos);
        if (mWordsList != null) {
            ((WordsCursorAdapter) mWordsList.getAdapter()).setSelected(pos);
        }
    }

    private void setSelectedCurrentDicWord(int pos) {
        mSelectedCurrentWordId = (int) mCurrentWordsList.getAdapter().getItemId(pos);
        if (mWordsList != null) {
            ((WordsCursorAdapter) mCurrentWordsList.getAdapter()).setSelected(pos);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mCurrentWordsManager != null)
            mCurrentWordsManager.close();
        if (mWordsDbManager != null)
            mWordsDbManager.close();

    }
}
