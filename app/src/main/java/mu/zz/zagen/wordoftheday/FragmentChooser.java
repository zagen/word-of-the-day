package mu.zz.zagen.wordoftheday;

import android.content.Context;
import android.support.v4.app.Fragment;

import java.util.HashMap;

import mu.zz.zagen.wordoftheday.fragments.BuyDictionaryInfoFragment;
import mu.zz.zagen.wordoftheday.fragments.CurrentDictionaryFragment;
import mu.zz.zagen.wordoftheday.fragments.CurrentWordFragment;
import mu.zz.zagen.wordoftheday.fragments.DictionariesFragment;
import mu.zz.zagen.wordoftheday.fragments.LoadDictionaryFragment;

/**
 * Created by zagen on 1/21/2015.
 */
public class FragmentChooser {
    private static HashMap<String, Class<? extends Fragment>> fragmentClasses = null;

    static public Fragment chooseFragment(Context context, String name)
    {
        if(getClasses(context).containsKey(name)){
            Fragment fragment = null;
            try {
                fragment = getClasses(context).get(name).newInstance();
            }catch (Exception ex){}
            return  fragment;
        }
        return null;
    }
    static HashMap<String, Class<? extends Fragment>> getClasses(Context context){
        if(fragmentClasses == null)
        {
            fragmentClasses = new HashMap<>();
            fragmentClasses.put(context.getString(R.string.title_section_today_word), CurrentWordFragment.class);
            fragmentClasses.put(context.getString(R.string.title_section_dictionaries), DictionariesFragment.class);
            fragmentClasses.put(context.getString(R.string.title_section_current_dictionary), CurrentDictionaryFragment.class);
            fragmentClasses.put(context.getString(R.string.title_section_load_dictionaries), LoadDictionaryFragment.class);
            fragmentClasses.put(context.getString(R.string.title_section_buy_dictionaries), BuyDictionaryInfoFragment.class);
        }
        return  fragmentClasses;
    }
}
