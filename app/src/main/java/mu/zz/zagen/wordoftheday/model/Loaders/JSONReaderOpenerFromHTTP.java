package mu.zz.zagen.wordoftheday.model.Loaders;

import com.google.gson.stream.JsonReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by zagen on 1/24/2015.
 */
public class JSONReaderOpenerFromHTTP implements JSONReaderOpener {
    @Override
    public JsonReader open(String uri) {
        HttpResponse response = null;
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet request = new HttpGet(uri);
            response = httpClient.execute(request);

            if (response.getStatusLine().getStatusCode() == 200) {
                HttpEntity responseEntity = response.getEntity();
                InputStream in = responseEntity.getContent();
                return new JsonReader(
                        new InputStreamReader(
                                in,
                                "UTF-8"));
            } else {

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
