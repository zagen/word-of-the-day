package mu.zz.zagen.wordoftheday.fragments;


import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import mu.zz.zagen.wordoftheday.AutoResizeTextView;
import mu.zz.zagen.wordoftheday.R;
import mu.zz.zagen.wordoftheday.WordOfTheDaySettings;
import mu.zz.zagen.wordoftheday.WordOfTheDayWidget;
import mu.zz.zagen.wordoftheday.model.CurrentDictionaryDbManager;
import mu.zz.zagen.wordoftheday.model.WordsDbManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class CurrentWordFragment extends Fragment implements View.OnClickListener {

    private WordsDbManager mWordsManager;
    private CurrentDictionaryDbManager mCurrentDicWordsManager;

    private TextView mWordTextView;
    private TextView mDescriptionTextView;

    public CurrentWordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_current_word, container, false);
        mWordTextView = (TextView) view.findViewById(R.id.word_of_the_day_textview);
        WordOfTheDaySettings settings = WordOfTheDaySettings.getInstance(getActivity());
        mWordTextView.setTypeface(settings.getBoldFont());

        mDescriptionTextView = (TextView) view.findViewById(R.id.word_of_the_day_description_textview);
        mDescriptionTextView.setTypeface(settings.getNormalFont());

        ImageButton reloadButton = (ImageButton)view.findViewById(R.id.reload_button);
        reloadButton.setOnClickListener(this);

        try {
            mWordsManager = new WordsDbManager(getActivity()).open();
        } catch (Exception e) {
        }

        try {
            mCurrentDicWordsManager = new CurrentDictionaryDbManager(getActivity()).open();
        } catch (Exception e) {
        }
        if(!settings.isTodayWordAlive())
            reloadWord();
        else {
            try {
                String wordId = settings.getTodayWord();
                if (wordId != null) {
                    WordsDbManager.Word word = mWordsManager.fetchWordInfo(wordId);
                    mWordTextView.setText(word.getName());
                    mDescriptionTextView.setText(word.getDescription());
                }
            } catch (Exception e) {
            }
        }
        return view;
    }

    private void reloadWord() {
        try {
            String wordId = mCurrentDicWordsManager.fetchRandomWordId();
            if (wordId != null) {
                WordOfTheDaySettings settings = WordOfTheDaySettings.getInstance(getActivity());
                WordsDbManager.Word word = mWordsManager.fetchWordInfo(wordId);
                mWordTextView.setText(word.getName());
                mDescriptionTextView.setText(word.getDescription());
                settings.setTodayWord(wordId);
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(mWordsManager!=null)
            mWordsManager.close();
        if(mCurrentDicWordsManager != null)
            mCurrentDicWordsManager.close();
    }

    @Override
    public void onClick(View v) {
        reloadWord();
    }
}
