package mu.zz.zagen.wordoftheday.model.Loaders;

import com.google.gson.stream.JsonReader;

/**
 * Created by zagen on 1/24/2015.
 */
public interface JSONReaderOpener {
    public JsonReader open(String uri);
}
