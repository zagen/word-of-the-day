package mu.zz.zagen.wordoftheday.model.Loaders;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;

import com.google.gson.stream.JsonReader;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by zagen on 1/26/2015.
 */
public class JSONReaderOpenerFromAssets implements JSONReaderOpener{
    private Context mContext;
    public JSONReaderOpenerFromAssets(Context context){
        this.mContext = context;
    }
    @Override
    public JsonReader open(String uri) {
        try
        {
            AssetManager am = mContext.getAssets();
            InputStream is = am.open(uri);
            return new JsonReader(new InputStreamReader(is));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
