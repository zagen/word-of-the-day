package mu.zz.zagen.wordoftheday.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import mu.zz.zagen.wordoftheday.R;
import mu.zz.zagen.wordoftheday.fragments.DictionaryOrderInfo.DictionaryOrderInfo;
import mu.zz.zagen.wordoftheday.fragments.DictionaryOrderInfo.DictionaryOrderInfoAdapter;
import mu.zz.zagen.wordoftheday.model.DictionariesDbManager;
import mu.zz.zagen.wordoftheday.model.Loaders.DictionaryLoader;
import mu.zz.zagen.wordoftheday.model.Loaders.JSONReaderOpenerFromFile;
import mu.zz.zagen.wordoftheday.model.Loaders.JSONReaderOpenerFromHTTP;

/**
 * A fragment representing a list of dictionary items.
 * <p/>
 * <p/>

 */
public class BuyDictionaryInfoFragment extends ListFragment implements DictionaryOrderInfoAdapter.OnItemButtonClickedListener, DictionaryLoader.OnLoadedListener {

    private DictionaryLoader mLoader;
    private ProgressDialog mWaitingDialog;

    private DictionariesDbManager mDicManager;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public BuyDictionaryInfoFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        List<DictionaryOrderInfo> dics = new ArrayList<>();
        DictionaryOrderInfo dic = new DictionaryOrderInfo();

        try {
            dic.setEntriesCount(2972);
            dic.setName("Словарь латинских фраз");
            dic.setUrl("http://zagen.zz.mu/dics/dictionary_of_latin_phrases.json");
            dics.add(dic);

            dic = new DictionaryOrderInfo();
            dic.setEntriesCount(534);
            dic.setName("Словарь умных слов");
            dic.setUrl("http://zagen.zz.mu/dics/dictionary_of_clever_words.json");
            dics.add(dic);
        }catch (Exception e){}

        setListAdapter(
                new DictionaryOrderInfoAdapter(
                        getActivity(),
                        R.layout.listfragment_buy_dictionary_item,
                        dics,
                        this));
        mLoader = new DictionaryLoader(getActivity(), new JSONReaderOpenerFromHTTP());
        mLoader.setOnLoadedListener(this);

        mWaitingDialog = new ProgressDialog(getActivity());
        mWaitingDialog.setTitle(R.string.wait_please);
        mWaitingDialog.setMessage(getString(R.string.loading));
        mWaitingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mWaitingDialog.setCancelable(false);
        mWaitingDialog.setButton(Dialog.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (mLoader != null)
                    mLoader.stopLoading();
            }
        });
        try{
            mDicManager = new DictionariesDbManager(getActivity()).open();
        }catch (Exception e){}

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mDicManager != null)
            mDicManager.close();
    }

    @Override
    public void onItemButtonClicked(int position) {
        if(isNetworkAvailable()){
            DictionaryOrderInfoAdapter adapter = (DictionaryOrderInfoAdapter)getListAdapter();
            String url = adapter.getItemUrl(position);
            String name = adapter.getName(position);

            if(mDicManager != null && mDicManager.isDbContainDicWithName(name)){
                alert(R.string.error, R.string.error_dic_already_exist);
            }else {
                if(mWaitingDialog != null)
                    mWaitingDialog.show();
                mLoader.load(name, url);
            }
        }else{
            alert(R.string.error, R.string.error_network_connection);
        }

    }

    @Override
    public void onLoaded(int words) {
        if(mWaitingDialog != null && mWaitingDialog.isShowing()){
            mWaitingDialog.dismiss();
        }
        Toast.makeText(getActivity(),getString(R.string.count_of_loaded_word) + Integer.toString(words), Toast.LENGTH_SHORT).show();
    }
    private void alert(int title, int message){
        new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(getString(message))
                .setPositiveButton(R.string.ok, null)
                .show();
    }
}
