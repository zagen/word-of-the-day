package mu.zz.zagen.wordoftheday.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.ArrayAdapter;

import java.util.Arrays;
import java.util.List;

import mu.zz.zagen.wordoftheday.R;
import mu.zz.zagen.wordoftheday.model.WordsCursorAdapter;
import mu.zz.zagen.wordoftheday.model.SpinnerStringAdapter;
import mu.zz.zagen.wordoftheday.WordOfTheDayActivity;
import mu.zz.zagen.wordoftheday.model.DictionariesDbManager;
import mu.zz.zagen.wordoftheday.model.WordsDbManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class DictionariesFragment extends Fragment implements
        View.OnClickListener,
        AdapterView.OnItemSelectedListener,
        AdapterView.OnItemClickListener{

    public final static String CURRENT_DICTIONARY = "current_dictionary";
    public final static String CURRENT_WORD_INDEX = "current_word";
    /**
     * Spinner for all dictionary list
     */
    private Spinner mDicSpinner;

    private ImageButton mEditButton;

    private ImageButton mDeleteButton;
    /**
     * listview, contain list of words for selected
     */
    private ListView mWordsList;
    private WordsDbManager mWordsDbManager;
    private DictionariesDbManager mDicManager;

    private String[] mDictionariesName = new String[]{};
    private String[] mDictionariesIds = new String[]{};

    private long mSelectedWordId = -1;

    private int mScrollTo = -1;

    /**
     * transform list of dictionary objects to 2 arrays of names and ids
     *
     * @param dics arrays of dictionaries
     */
    private void loadDictionaries(List<DictionariesDbManager.Dic> dics) {
        int count = dics.size();
        if (dics != null) {
            mDictionariesName = new String[count];
            mDictionariesIds = new String[count];
            int index = 0;
            for (DictionariesDbManager.Dic dic : dics) {
                mDictionariesName[index] = dic.getName();
                mDictionariesIds[index] = dic.getId();
                index++;
            }
            if (mDicSpinner != null) {
                ArrayAdapter<String> spinnerAdapter =
                        new SpinnerStringAdapter(getActivity(), R.layout.listfragment_spinner, mDictionariesName);
                spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mDicSpinner.setAdapter(spinnerAdapter);
                mDicSpinner.setOnItemSelectedListener(this);
            }
        }
    }

    public DictionariesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dictionaries, container, false);

        //take listView
        mWordsList = (ListView) view.findViewById(R.id.dictionary_content_list);
        WordsCursorAdapter listAdapter = new WordsCursorAdapter(getActivity(), null, true);
        mWordsList.setAdapter(listAdapter);
        mWordsList.setOnItemClickListener(this);

        this.mDicSpinner = (Spinner) view.findViewById(R.id.dictionaries_spinner);

        //set callbacks for click
        ImageButton addButton = (ImageButton) view.findViewById(R.id.add_entry_image_button);
        addButton.setOnClickListener(this);

        try {
            mDicManager = new DictionariesDbManager(getActivity()).open();
            loadDictionaries(mDicManager.fetchAllDictionaries());

            //create manager for words
            mWordsDbManager = new WordsDbManager(getActivity()).open();
        } catch (Exception e) {
        }

        mEditButton = (ImageButton) view.findViewById(R.id.edit_entry_image_button);
        mEditButton.setOnClickListener(this);
        mEditButton.setEnabled(false);

        mDeleteButton = (ImageButton) view.findViewById(R.id.remove_entry_image_button);
        mDeleteButton.setOnClickListener(this);
        mDeleteButton.setEnabled(false);

        ImageButton deleteDictionaryButton = (ImageButton) view.findViewById(R.id.remove_all_image_button);
        deleteDictionaryButton.setOnClickListener(this);
        loadCurrentDictionary();

        return view;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(mWordsDbManager != null)
            mWordsDbManager.close();
        if(mDicManager != null)
            mDicManager.close();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null) {
            Bundle args = getArguments();
            if (args.containsKey(CURRENT_DICTIONARY)) {
                String dictionary = getArguments().getString(CURRENT_DICTIONARY);
                int dicId = Arrays.asList(mDictionariesIds).indexOf(dictionary);
                mDicSpinner.setSelection(dicId);
                loadCurrentDictionary();
                if (args.containsKey(CURRENT_WORD_INDEX)) {
                    this.mScrollTo = args.getInt(CURRENT_WORD_INDEX);
                }
            }
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        if (mScrollTo != -1 && mWordsList != null) {
            mWordsList.setSelection(mScrollTo);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.remove_entry_image_button) {
            final String entryId = Long.toString(mSelectedWordId);
            new AlertDialog.Builder(getActivity())
                    .setTitle(getString(R.string.delete_conformation))
                    .setMessage(getString(R.string.delete_question))
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //if deleting isnt successful show message
                            if (!mWordsDbManager.deleteWord(entryId)) {
                                new AlertDialog.Builder(getActivity())
                                        .setTitle(getString(R.string.error))
                                        .setMessage(getString(R.string.error_delete_word_from_dic))
                                        .setPositiveButton(R.string.ok, null)
                                        .show();
                            } else {//otherwise reload cursor
                                loadCurrentDictionary();
                            }
                        }
                    })
                    .setNegativeButton(R.string.cancel, null)
                    .show();

        } else if (v.getId() == R.id.remove_all_image_button) {
            int spinnerPosition = mDicSpinner.getSelectedItemPosition();
            if (spinnerPosition != -1)
                new AlertDialog.Builder(getActivity())
                        .setTitle(getString(R.string.delete_conformation))
                        .setMessage(getString(R.string.delete_dic_question))
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    if (mDicManager != null) {
                                        String dicId = mDictionariesIds[mDicSpinner.getSelectedItemPosition()];
                                        mDicManager.deleteDictionary(dicId);
                                        loadDictionaries(mDicManager.fetchAllDictionaries());
                                        loadCurrentDictionary();

                                    }
                                } catch (Exception e) {
                                }
                            }
                        })
                        .setNegativeButton(R.string.cancel, null)
                        .show();
        } else {
            openAddEditFragment(v.getId() == R.id.add_entry_image_button);
        }
    }

    private void openAddEditFragment(boolean forAdd){
        WordOfTheDayActivity activity = (WordOfTheDayActivity) getActivity();
        FragmentManager fragmentManager = activity.getSupportFragmentManager();

        int spinnerPosition = mDicSpinner.getSelectedItemPosition();
        String dicID = "-1";
        String entryId = Long.toString(mSelectedWordId);
        try {
            dicID = mDictionariesIds[spinnerPosition];
        } catch (Exception e) {
        }

        if (forAdd) {
            entryId = "-1";
        }
        int position = mWordsList.getFirstVisiblePosition();
        Fragment fragment =
                EntryAddOrEditFragment.newInstance(
                        dicID,
                        entryId);
        if (position != -1)
            fragment =
                    EntryAddOrEditFragment.newInstance(
                            dicID,
                            entryId,
                            position);

        if (!dicID.equals("-1"))
            fragmentManager.beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit();
    }

    private void setSelectedCurrentDicWord(int pos) {
        mSelectedWordId = (int) mWordsList.getAdapter().getItemId(pos);
        if (mWordsList != null) {
            ((WordsCursorAdapter) mWordsList.getAdapter()).setSelected(pos);
        }
    }

    private void loadCurrentDictionary() {
        Cursor dictionariesCursor = null;
        try {
            int spinnerIndex = mDicSpinner.getSelectedItemPosition();
            setSelectedCurrentDicWord(-1);

            if (spinnerIndex != -1)
                dictionariesCursor = mWordsDbManager.fetchWordsFromDictionary(mDictionariesIds[spinnerIndex]);
            CursorAdapter adapter = (CursorAdapter) mWordsList.getAdapter();
            adapter.changeCursor(dictionariesCursor);
        } catch (Exception ex) {
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        loadCurrentDictionary();
        setSelectedCurrentDicWord(-1);
        if (mEditButton != null)
            mEditButton.setEnabled(false);
        if (mDeleteButton != null)
            mDeleteButton.setEnabled(false);

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long arg3) {

        if(mSelectedWordId == (int) mWordsList.getAdapter().getItemId(position)){
            openAddEditFragment(false);
        }else{
            setSelectedCurrentDicWord(position);
            if (mEditButton != null)
                mEditButton.setEnabled(true);
            if (mDeleteButton != null)
                mDeleteButton.setEnabled(true);
            for (int a = 0; a < parent.getChildCount(); a++) {
                parent.getChildAt(a).setBackgroundColor(Color.TRANSPARENT);
            }
            view.setBackgroundColor(Color.LTGRAY);
        }

    }
}
