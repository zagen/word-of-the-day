package mu.zz.zagen.wordoftheday.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mu.zz.zagen.wordoftheday.WordOfTheDaySettings;

/**
 * Created by zagen on 1/23/2015.
 */
public class DictionariesDbManager {
    private WordsDatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;
    private Context mContext;

    public class Dic {
        private String mId;
        private String mName;

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            this.mName = name;
        }

        public String getId() {
            return mId;
        }

        public void setId(String id) {
            this.mId = id;
        }
    }

    public DictionariesDbManager(Context context) {
        mContext = context;
    }

    public DictionariesDbManager open() throws SQLException {
        mDbHelper = new WordsDatabaseHelper(mContext);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        if (mDbHelper != null) {
            mDbHelper.close();
        }
    }

    public long addDictionary(String name) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(WordsDatabaseHelper.KEY_DICTIONARIES_NAME, name);
        return mDb.insert(WordsDatabaseHelper.TABLE_DICTIONARIES, null, initialValues);
    }

    public boolean deleteDictionary(String id) {
        //check if this dic contain current word;
        WordOfTheDaySettings settings = WordOfTheDaySettings.getInstance(mContext);
        if (settings.isTodayWordAlive()) {
            String wordId = settings.getTodayWord();
            Cursor cursor = mDb.query(
                    WordsDatabaseHelper.TABLE_WORDS,
                    new String[]{WordsDatabaseHelper.KEY_WORDS_DIC},
                    WordsDatabaseHelper.KEY_WORDS_ID + " = ?",
                    new String[]{wordId}, null, null, null
            );
            if (cursor != null) {
                cursor.moveToFirst();
                if (cursor.getCount() > 0) {
                    String dicForTodayWord = cursor.getString(cursor.getColumnIndex(WordsDatabaseHelper.KEY_WORDS_DIC));
                    if (dicForTodayWord.equals(id))
                        settings.resetTodayWord();
                }
            }
        }
        int doneDelete = 0;
        doneDelete = mDb.delete(
                WordsDatabaseHelper.TABLE_DICTIONARIES,
                WordsDatabaseHelper.KEY_DICTIONARIES_ID + "=?",
                new String[]{id});

        return doneDelete > 0;
    }

    public List<Dic> fetchAllDictionaries() {

        List<Dic> dics = new ArrayList<>();
        Cursor mCursor = mDb.query(WordsDatabaseHelper.TABLE_DICTIONARIES,
                new String[]{
                        WordsDatabaseHelper.KEY_DICTIONARIES_ID,
                        WordsDatabaseHelper.KEY_DICTIONARIES_NAME},
                null, null, null, null, null);

        if (mCursor != null) {
            // looping through all rows and adding to list

            while (mCursor.moveToNext()) {
                Dic dic = new Dic();
                dic.setId(mCursor.getString(mCursor.getColumnIndex(WordsDatabaseHelper.KEY_DICTIONARIES_ID)));
                dic.setName(mCursor.getString(mCursor.getColumnIndex(WordsDatabaseHelper.KEY_DICTIONARIES_NAME)));
                dics.add(dic);
            }
        }
    return dics;
}

    public String fetchDictionaryName(String id) throws SQLException {
        Cursor mCursor = mDb.query(true,
                WordsDatabaseHelper.TABLE_DICTIONARIES,
                new String[]{
                        WordsDatabaseHelper.KEY_DICTIONARIES_NAME},
                WordsDatabaseHelper.KEY_DICTIONARIES_ID + "= ?",
                new String[]{
                        id
                },
                null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor.getString(0);
    }

    public boolean isDbContainDicWithName(String name) {
        Cursor mCursor = mDb.query(true,
                WordsDatabaseHelper.TABLE_DICTIONARIES,
                null,
                WordsDatabaseHelper.KEY_DICTIONARIES_NAME + "= ?",
                new String[]{
                        name
                },
                null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
            return mCursor.getCount() > 0;
        }
        return false;

    }

}
