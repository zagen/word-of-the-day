package mu.zz.zagen.wordoftheday.model;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import mu.zz.zagen.wordoftheday.WordOfTheDaySettings;

/**
 * Created by zagen on 1/23/2015.
 */
public class SpinnerStringAdapter extends ArrayAdapter<String> {
    String[] mItems;
    public SpinnerStringAdapter(Context context,  int textViewResourceId, String[] objects) {
        super(context, textViewResourceId, objects);
        mItems = objects;
    }
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = super.getView(position, convertView, parent);

        Typeface externalFont= WordOfTheDaySettings.getInstance(getContext()).getBoldFont();
        ((TextView) v).setTypeface(externalFont);

        return v;
    }


    public View getDropDownView(int position,  View convertView,  ViewGroup parent) {
        View v = super.getDropDownView(position, convertView, parent);

        Typeface externalFont=WordOfTheDaySettings.getInstance(getContext()).getBoldFont();
        ((TextView) v).setTypeface(externalFont);
        return v;
    }
}
