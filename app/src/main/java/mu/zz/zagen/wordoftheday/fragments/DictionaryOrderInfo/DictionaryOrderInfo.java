package mu.zz.zagen.wordoftheday.fragments.DictionaryOrderInfo;

import java.net.URL;

/**
 * Created by zagen on 1/22/2015.
 */
public class DictionaryOrderInfo {
    private String mName;
    private int mEntriesCount;
    private String mProductId;
    private String mUrl;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public int getEntriesCount() {
        return mEntriesCount;
    }

    public void setEntriesCount(int entriesCount) {
        this.mEntriesCount = entriesCount;
    }

    public String getProductId() {
        return mProductId;
    }

    public void setProductId(String productId) {
        this.mProductId = productId;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        this.mUrl = url;
    }
}
