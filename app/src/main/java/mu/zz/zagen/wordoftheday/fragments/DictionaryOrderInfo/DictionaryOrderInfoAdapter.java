package mu.zz.zagen.wordoftheday.fragments.DictionaryOrderInfo;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import mu.zz.zagen.wordoftheday.R;
import mu.zz.zagen.wordoftheday.WordOfTheDaySettings;

/**
 * Created by zagen on 1/22/2015.
 */
public class DictionaryOrderInfoAdapter extends ArrayAdapter<DictionaryOrderInfo> {
    private List<DictionaryOrderInfo> mDictionaries;

    public interface OnItemButtonClickedListener{
        public void onItemButtonClicked(int position);
    }
    private OnItemButtonClickedListener mCallback;
    public DictionaryOrderInfoAdapter(Context context,
                                      int resource,
                                      List<DictionaryOrderInfo> objects,
                                      OnItemButtonClickedListener callback) {
        super(context, resource, objects);
        mDictionaries = objects;
        mCallback = callback;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listfragment_buy_dictionary_item, null);
            ImageButton downloadButton = (ImageButton)view.findViewById(R.id.buy_dictionary_button);
            downloadButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mCallback != null){
                        mCallback.onItemButtonClicked(position);
                    }
                }
            });
        }


        DictionaryOrderInfo info = mDictionaries.get(position);
        if (info != null) {
            TextView nameTextView = (TextView) view.findViewById(R.id.buy_dic_item_name);
            Typeface tfBold = WordOfTheDaySettings.getInstance(getContext()).getBoldFont();
            Typeface tfNormal = WordOfTheDaySettings.getInstance(getContext()).getNormalFont();
            TextView countTextView = (TextView) view.findViewById(R.id.buy_dic_item_count);
            try{
                nameTextView.setText(info.getName());
                nameTextView.setTypeface(tfBold);
            }catch (Exception e){}
            try {
                countTextView.setText(getContext().getString(R.string.entries_count)
                        + " : " + Integer.toString(info.getEntriesCount()));
                countTextView.setTypeface(tfNormal);
            }catch (Exception e){}

        }
        return view;
    }
    public String getItemUrl(int position){
        if(position < mDictionaries.size())
            return mDictionaries.get(position).getUrl();
        else return null;
    }
    public String getName(int position){
        if(position < mDictionaries.size())
            return mDictionaries.get(position).getName();
        else return null;
    }
}
