package mu.zz.zagen.wordoftheday;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Environment;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by zagen on 1/21/2015.
 */
public class FileListView extends ListView {
    private String mPath = Environment.getExternalStorageDirectory().getPath();;

    /**
     * list of files of currentPath
     */
    private List<File> files = new ArrayList<File>();

    /**
     * filter of filename.
     */
    private FilenameFilter mFilenameFilter;

    /**
     * index of selected list item
     */
    private int mSelectedIndex = -1;

    private TextView mTitleTextView;

    public void setTitleTextView(TextView textView)
    {
        this.mTitleTextView = textView;
    }

    private void init(){

        setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                final ArrayAdapter<File> adapter = (FileAdapter) adapterView.getAdapter();
                File file = adapter.getItem(index);
                if (file.toString().equals("..")) {
                    file = new File(mPath).getParentFile();
                    if (file != null) {
                        mPath = file.getPath();
                        if(mDirectoryChangedCallback != null){
                            mDirectoryChangedCallback.onDirectoryChanged(mPath);
                        }
                        RebuildFiles(adapter);
                    }
                } else if (file.isDirectory()) {
                    mPath = file.getPath();
                    if(mDirectoryChangedCallback != null){
                        mDirectoryChangedCallback.onDirectoryChanged(mPath);
                    }
                    RebuildFiles(adapter);
                } else {
                    if (index != mSelectedIndex)
                        mSelectedIndex = index;
                    else
                        mSelectedIndex = -1;
                    adapter.notifyDataSetChanged();
                    if(mFileSelectedCallback != null) {
                        String path = file.getPath();
                        mFileSelectedCallback.onFileSelected(path);
                    }
                }
            }
        });
        setBackgroundColor(Color.TRANSPARENT);

        files.addAll(getFiles(mPath));
        setAdapter(new FileAdapter(getContext(), files));
        setFilter("(.*)\\.json");
    }

    /**
     * refill list of file for currentPath
     * @param adapter
     */
    private void RebuildFiles(ArrayAdapter<File> adapter) {
        try {
            List<File> fileList = getFiles(mPath);
            files.clear();
            files.addAll(fileList);
            adapter.notifyDataSetChanged();
            changeTitle(mTitleTextView);
        } catch (NullPointerException e) {
        }
    }
    /**
     * get list of all file in directoryPath + ".."
     * @param directoryPath
     * @return
     */
    private List<File> getFiles(String directoryPath) {
        File directory = new File(directoryPath);
        List<File>fileList =  new ArrayList<File>();
        try {
            fileList.add(new File(".."));
            fileList.addAll(Arrays.asList(directory.listFiles(mFilenameFilter)));
            Collections.sort(fileList, new Comparator<File>() {
                @Override
                public int compare(File file, File file2) {
                    if (file.isDirectory() && file2.isFile())
                        return -1;
                    else if (file.isFile() && file2.isDirectory())
                        return 1;
                    else
                        return file.getPath().compareTo(file2.getPath());
                }
            });
        } catch (Exception e) {
        }
        return fileList;
    }
    private class FileAdapter extends ArrayAdapter<File> {

        public FileAdapter(Context context, List<File> files) {
            super(context, android.R.layout.simple_list_item_1, files);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView view = (TextView) super.getView(position, convertView, parent);
            view.setTypeface(WordOfTheDaySettings.getInstance(getContext()).getNormalFont());
            File file = getItem(position);
            if (view != null) {
                view.setText(file.getName());
                view.setTextColor(Color.BLACK);
                if (!file.isDirectory()) {
                    if (mSelectedIndex == position)
                        view.setBackgroundColor(Color.LTGRAY);
                    else
                        view.setBackgroundColor(Color.TRANSPARENT);
                } else {
                    view.setBackgroundColor(Color.TRANSPARENT);
                }
            }
            return view;
        }
    }

    public FileListView(Context context) {
        super(context);
        init();
    }

    public FileListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FileListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    /**
     * set filter for showing files(regular expression)
     * @param filter
     * @return
     */
    public void setFilter(final String filter) {
        mFilenameFilter = new FilenameFilter() {

            @Override
            public boolean accept(File file, String fileName) {
                File tempFile = new File(String.format("%s/%s", file.getPath(), fileName));

                try {
                    if(tempFile.isFile())
                        return tempFile.getName().matches(filter);
                } catch (Exception e) {
                    return false;
                }
                return true;
            }
        };
    }
    /**
     * change title of Title textView. set like name of currentPath. if it need cut it off
     */
    private void changeTitle(TextView title) {
        int maxWidth = 20;
        String titleText = mPath;
        if (titleText.length() > maxWidth) {
            while (titleText.length() > maxWidth) {
                int start = titleText.indexOf("/", 2);
                if (start > 0)
                    titleText = titleText.substring(start);
                else
                    titleText = titleText.substring(2);
            }
            title.setText("..." + titleText);
        } else {
            title.setText(titleText);
        }
    }

    public void setPath(String path)
    {
        File dir = new File(path);
        if(dir.exists() && dir.isDirectory()) {
            mPath = path;
            FileAdapter fa = (FileAdapter)getAdapter();
            RebuildFiles(fa);
        }
    }
    public String getPath(){
        return mPath;
    }
    public interface OnDirectoryChangedListener{
        public  void onDirectoryChanged(String path);
    }
    public interface OnFileSelectedListener{
        public  void onFileSelected(String path);
    }

    private OnDirectoryChangedListener mDirectoryChangedCallback;
    private OnFileSelectedListener mFileSelectedCallback;

    public void setOnDirectoryChangedListener(OnDirectoryChangedListener callback){
        this.mDirectoryChangedCallback = callback;
    }
    public void setOnFileSelectedListener(OnFileSelectedListener callback){
        this.mFileSelectedCallback = callback;
    }
}
