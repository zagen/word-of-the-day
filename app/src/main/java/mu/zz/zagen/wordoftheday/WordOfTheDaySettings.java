package mu.zz.zagen.wordoftheday;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by zagen on 1/22/2015.
 */
public class WordOfTheDaySettings {
    private static WordOfTheDaySettings ourInstance = new WordOfTheDaySettings();
    private static Typeface mAppFontBold = null;
    private static Typeface mAppFontNormal = null;
    private static SharedPreferences mPreferences;

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private final static String PREFERENCES_NAME = "mu.zz.zagen.wordoftheday.sharedpreferences";
    private final static String PREFERENCE_WORD = "mu.zz.zagen.wordoftheday.sharedpreferences.currentword";
    private final static String PREFERENCE_DATE = "mu.zz.zagen.wordoftheday.sharedpreferences.changedate";

    public static WordOfTheDaySettings getInstance(Context context) {
        if(mAppFontBold == null || mAppFontNormal == null){
            mAppFontBold = Typeface.createFromAsset(context.getAssets(), "fonts/unisans_heavy.ttf");
            mAppFontNormal = Typeface.createFromAsset(context.getAssets(), "fonts/kelsonsans _regular.ttf");

        }
        if(mPreferences == null)
            mPreferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        return ourInstance;
    }

    private WordOfTheDaySettings() {
    }
    public Typeface getBoldFont(){
        return mAppFontBold;
    }

    public Typeface getNormalFont(){
        return mAppFontNormal;
    }

    public boolean isTodayWordAlive(){

        final long SECONDS_IN_ONE_DAY = 86400;
        try{
            if(mPreferences.contains(PREFERENCE_DATE)){

                Calendar calendar = Calendar.getInstance();
                Date date = simpleDateFormat.parse(mPreferences.getString(PREFERENCE_DATE, null));
                Date currentDate = calendar.getTime();
                long diffInSec = TimeUnit.MILLISECONDS.toSeconds(currentDate.getTime() - date.getTime());
                return diffInSec < SECONDS_IN_ONE_DAY;
            }
        }catch (Exception e){}
        return  false;
    }
    public String getTodayWord(){
        if(isTodayWordAlive())
            return mPreferences.getString(PREFERENCE_WORD, null);
        return null;
    }
    public void setTodayWord(String word){
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(PREFERENCE_DATE, simpleDateFormat.format(date));
        editor.putString(PREFERENCE_WORD, word);
        editor.commit();
    }
    public void resetTodayWord(){
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.remove(PREFERENCE_WORD);
        editor.remove(PREFERENCE_DATE);
        editor.commit();
    }
}
