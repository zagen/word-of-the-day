package mu.zz.zagen.wordoftheday.model;

import android.content.ContentValues;
import android.content.Context;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;

import java.sql.SQLException;
import java.util.Random;

import mu.zz.zagen.wordoftheday.WordOfTheDaySettings;

/**
 * Created by zagen on 1/23/2015.
 */
public class CurrentDictionaryDbManager {
    private WordsDatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;
    private Context mContext;
    private WordOfTheDaySettings mSettings;
    private boolean mIsLoading = false;

    public void stop() {
        mIsLoading = false;
    }

    public CurrentDictionaryDbManager(Context context) {
        mContext = context;
        mSettings = WordOfTheDaySettings.getInstance(context);
    }

    public CurrentDictionaryDbManager open() throws SQLException {
        mDbHelper = new WordsDatabaseHelper(mContext);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public synchronized void close() {
        if (mDbHelper != null) {
            mDbHelper.close();
        }
    }

    public long addCurrentDictionaryWord(String id) {
        Cursor cursor = mDb.query(WordsDatabaseHelper.TABLE_WORDS,
                null,
                WordsDatabaseHelper.KEY_WORDS_ID + "= ?",
                new String[]{
                        id
                }, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                cursor.close();
                ContentValues initialValues = new ContentValues();
                initialValues.put(WordsDatabaseHelper.KEY_CURRENT_DICTIONARY_ID, id);
                return mDb.insert(WordsDatabaseHelper.TABLE_CURRENT_DICTIONARY, null, initialValues);
            }
        }
        return -1;

    }

    public boolean deleteWord(String id) {
        int doneDelete = 0;
        doneDelete = mDb.delete(WordsDatabaseHelper.TABLE_CURRENT_DICTIONARY,
                WordsDatabaseHelper.KEY_CURRENT_DICTIONARY_ID + "= ?",
                new String[]{
                        id
                });
        if (doneDelete > 0 && id.equals(mSettings.getTodayWord()))
            mSettings.resetTodayWord();

        return doneDelete > 0;
    }

    public boolean deleteAllCurrentDictionaryWords() {
        int doneDelete = 0;
        doneDelete = mDb.delete(WordsDatabaseHelper.TABLE_CURRENT_DICTIONARY, null, null);
        mSettings.resetTodayWord();
        return doneDelete > 0;
    }

    public Cursor fetchAllCurrentDictionaryWords() {

        Cursor mCursor = mDb.query(WordsDatabaseHelper.TABLE_WORDS,
                new String[]{
                        WordsDatabaseHelper.KEY_WORDS_ID,
                        WordsDatabaseHelper.KEY_WORDS_DIC,
                        WordsDatabaseHelper.KEY_WORDS_NAME,
                        WordsDatabaseHelper.KEY_WORDS_DESC
                },
                WordsDatabaseHelper.KEY_WORDS_ID + " IN (SELECT " + WordsDatabaseHelper.KEY_CURRENT_DICTIONARY_ID + " from " +
                        WordsDatabaseHelper.TABLE_CURRENT_DICTIONARY + ")",
                null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public boolean isWordInCurrentAlready(String id) {
        boolean isCurrent = true;
        Cursor cursor = null;
        try {
            cursor = mDb.query(
                    WordsDatabaseHelper.TABLE_CURRENT_DICTIONARY,
                    null,
                    WordsDatabaseHelper.KEY_CURRENT_DICTIONARY_ID + "=?",
                    new String[]{id}, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                if (cursor.getCount() <= 0) {
                    isCurrent = false;
                }
            } else
                isCurrent = false;
        } catch (Exception e) {
        } finally {
            if (cursor != null)
                cursor.close();
        }


        return isCurrent;
    }

    public interface OnLoadCompleteListener {
        void onLoadComplete();
    }

    public void moveAllDicWordsToCurrent(final String dicId, final OnLoadCompleteListener callback) {

        //start new thread where we add all words to current dictionary
       /* new Thread(new Runnable() {
            @Override
            public void run() {*/
                WordsDbManager wordsDbManager = null;
                try {
                    wordsDbManager = new WordsDbManager(mContext).open();
                    //make query and select all words ids for selected dic

                    Cursor cursor = wordsDbManager.fetchWordsFromDictionary(dicId);
                    mIsLoading = true;
                    try {
                        while (cursor.moveToNext()) {
                            if (mIsLoading) {
                                //put in loop words to current dic, watch if there is no interruption
                                String idWord = cursor.getString(cursor.getColumnIndex(WordsDatabaseHelper.KEY_WORDS_ID));
                                addCurrentDictionaryWord(idWord);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        if (cursor != null)
                            cursor.close();
                    }


                } catch (Exception e) {
                } finally {
                    if (wordsDbManager != null)
                        wordsDbManager.close();
                }
                //run code in main thread
                new Handler(mContext.getMainLooper())
                        .post(new Runnable() {
                            @Override
                            public void run() {
                                if (callback != null)
                                    callback.onLoadComplete();
                            }
                        });

            //}
        //}).start();
    }

    public String fetchRandomWordId() {
        String count;
        Cursor cursor = mDb.rawQuery(
                "SELECT COUNT(*) FROM " + WordsDatabaseHelper.TABLE_CURRENT_DICTIONARY,
                null);
        if (cursor != null && cursor.moveToNext()) {
            count = cursor.getString(0);
            Random random = new Random();
            int currentDicWordsCount = Integer.parseInt(count);
            if (currentDicWordsCount <= 0)
                return null;
            String randNumber = Integer.toString(random.nextInt(currentDicWordsCount));
            String query = "SELECT " + WordsDatabaseHelper.KEY_CURRENT_DICTIONARY_ID +
                    " FROM " + WordsDatabaseHelper.TABLE_CURRENT_DICTIONARY +
                    " LIMIT " + randNumber + " , 1";
            cursor = mDb.rawQuery(query, null);
            if (cursor != null && cursor.moveToNext()) {
                return cursor.getString(0);
            }


        }

        return null;
    }
}
